// uni.request请求封装
// const baseUrl = 'http://www.qd20231105.test/';//请求的根路径
const baseUrl = 'https://work.bittrano.com/api/';
// const baseUrl = 'http://www.newadmin.test/api/';
//图片地址
const imgUrl = 'https://work.bittrano.com';
// const imgUrl = 'http://www.newadmin.test';
const h5Url = 'https://mobiles.bittrano.com/h5/';
let token = '';//token令牌
const request = (options = {}) => {
	uni.getStorageSync('token') &&(token = uni.getStorageSync('token'));//从本地缓存中获取token
	return new Promise((resolve, reject)=>{
		// 配置默认请求头
		options.header = {
			 'Authorization': `${token}`,
			 // 'language': uni.getStorageSync('language'),
		};
		uni.showLoading({
			title: 'loading....'
		});
		// options.data.lang = uni.getStorageSync('language')
		let lang = uni.getStorageSync('language');
		let langs = 'en-www';
		if(lang == 'tr'){
			langs = 'tr-tr'
		}else if(lang == 'en'){
			langs = 'en-www'
		}else if(lang == 'jp'){
			langs = 'jr-rbg'
		}else if(lang == 'zh-CN'){
			langs = 'zh-cn'
		}else if(lang == 'ar'){
			langs = 'ar-oa'
		}else if(lang == 'es'){
			langs = 'en-id'
		}
		uni.request({
			url: baseUrl + options.url+'?lang='+langs || '',
			method: options.method || 'GET', 
			data: options.data || {},
			header: options.header || {},
			success: (res) => {
				uni.hideLoading();
				if (res.data.code == 200) {//成功
		            resolve(res.data, res);
		        }else{
					if (res.data.code == 401) {//未登录
						uni.showToast({
							title:'Unauthorized',
							icon: 'none',
							duration: 1500
						});
						setTimeout(function(){
							uni.reLaunch({
							    url: "/pages/login/login"
							});
						},1500);
						return;
					}
					if (res.data.code == 423) {//未实名
						uni.showToast({
							title:res.data.msg,
							icon: 'error',
							duration: 1500
						});
						setTimeout(function(){
							uni.navigateTo({
							    url: "/pages/mine/reallyName"
							});
						},1500);
						return;
					}
					uni.showToast({
						title:res.data.msg,
						icon: 'none',
						duration: 1500
					});
					return;
				}
			},
		    fail: (msg) => {
				console.log(888888)
				uni.hideLoading();
				uni.showToast({
				    title: 'error',
				    icon: 'none',
				    duration: 1500
				});
		        // reject('请求失败');
		    },
			complete() {
				uni.hideLoading();
			}
		  
		})
  })
}
 
// GET请求
const get = (url, data={}, options={}) => {
  options.url = url
  options.method = 'GET'
  options.data = data
  return request(options)
}
 
// POST请求
const post = (url, data={}, options={}) => {
  options.url = url
  options.method = 'POST'
  options.data = data
  return request(options)
}
 
// 默认向外暴露的数据
export default  {
    request,
	baseUrl,
	imgUrl,
	h5Url,
    get,
    post
}